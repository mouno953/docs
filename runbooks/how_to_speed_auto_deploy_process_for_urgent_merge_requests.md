# How to speed the auto-deploy process for urgent merge requests?

Specific situations such as mitigating incidents or unblocking the security release,
could require for a specific merge request (most likely a revert) to be deployed as
quickly as possible. Instead of waiting for a new auto-deploy branch to be created,
which could be time-consuming, there are some steps the release manager can follow to
speed up the auto-deploy process:

1. Cherry-pick the merge request into the latest auto-deploy branch by
   adding `~Pick into auto-deploy` and the `severity::2` or higher labels.
1. Trigger the [`auto_deploy:pick`] [pipeline schedule].
1. Enable the [`auto_deploy_tag_latest`] feature flag.
1. Trigger the [`auto_deploy:tag`] [pipeline schedule]. This will create
   a coordinated pipeline with the merge request that needs to be deployed.

Once the merge request in the auto-deploy branch has a green pipeline, disable the
[`auto_deploy_tag_latest`] feature flag, disabling the flag before could lead to
tagging an auto-deploy package using an earlier commit than the required merge request.

[`auto_deploy:pick`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/72/edit
[`auto_deploy:tag`]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/74/edit
[`auto_deploy_tag_latest`]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/191/edit
[pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
